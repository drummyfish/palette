**update**: New and improved palette is [here](https://gitlab.com/drummyfish/palette2).

# general purpose HSV-based 256 color palette

This is a general purpose 256 color palette I have created mostly for the use in my Pokitto and Gamebuino META demos. (Actually I have modified it slightly, to not have duplicite colors.) Its main advantage is that it can relatively quickly change brightness (value) of colors, which you can use to do lighting/shading effects (the function is below) or fading into dark/light:

![](https://gitlab.com/drummyfish/raycastlib/raw/master/media/pokitto1.gif)
![](media/texture.gif)

The original Doom used a similar approach for shading the levels with a limited palette, but they used mapping tables -- the palette I am sharing doesn't need any tables (wasted memory), the easy shading is built into it by ordering the colors in a specific way.

Changing hue and saturation can be done similarly easily (by adding 16 or 128 to the index, respectively).

The palette is based on the HSV model and consists of sections of different hues in which the value increases, which means that to change a value (brightness) of a color you can simply increment or decrement the color index. Here is some documentation:

![](media/principle.png)

The disadvantage is that the colors often aren't very faithful to the original image -- you'll find much better palettes for that -- and since there are just three levels of saturation (because most bits are reserved for value), you can go either for very saturated or little saturated, but not much between.

You can still achieve nice results by choosing convenient images and using things like dithering, e.g. (original vs palette):

![](media/trees.png)

There are two versions of the palette: RGB8 and 565 -- RGB8 is the original one, the 565 is the RGB8 converted to 565 format (often used to save space). Since after a plain conversion there end up being some duplicite colors, I modified it to have all colors unique.

- RGB8: ![](media/palette.png) 
- RGB565: ![](media/palette565.png)

# how to use

In any way you want, but typically, e.g. on Pokitto or Gamebuino, include `palette.h` in your project and choose which version (RGB8 or RGB565) you want to use by changing the define at the begining of that file.

The file contains some helper functions, mainly the ones used for changing brightness. Take a look at the file and the comments.

You may want to convert your images to use this palette -- for that you can use various tools, such as [this one](https://gitlab.com/drummyfish/small3dlib/blob/master/tools/img2array.py) that I have written.

On Pokitto or Gamebuino META you need to load a palette to start using it. You do it like this:

```
Pokitto::Core::display.load565Palette(palette565); // must be done AFTER calling begin()
```

```
Gamebuino_Meta::Color paletteGB[256]; // global variable

// ...

for (uint16_t i = 0; i < 256; ++i)
  paletteGB[i] = gb.createColor(palette[i * 3],palette[i * 3 + 1],palette[i * 3 + 2]);
```

# usage rights

I release the palettes and everything in this repository under **CC0 1.0 (public domain)**. (Example images were made by me, the font used in the documentation is Aileron, CC0.)