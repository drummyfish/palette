# General purpose HSV-based 256 color palette.
#
# by Drummyfish, released under CC0 1.0

import colorsys
from PIL import Image
import textwrap

def clampToByte(value):
  value = int(value)
  return 0 if value < 0 else (255 if value > 255 else value)

def rgbTo565(r, g, b):
  r = r >> 3
  g = g >> 2
  b = b >> 3
  
  return (r << 11) | (g << 5) | b 

def rgbFrom565(rgb565):
  return (
    ((rgb565 & int('1111100000000000',2)) >> 11) << 3,
    ((rgb565 & int('0000011111100000',2)) >> 5) << 2,
    (rgb565 & int('0000000000011111',2)) << 3)

def paletteIndexToHSV(i):
  i = clampToByte(i)

  topHalf = i & int('11110000',2)
  bottomHalf = i & int('00001111',2)
  
  if topHalf != 0:
    # "normal" color
    h = ((i & int('01110000',2)) >> 4) * 32
    s = (1 + ((i & int('10000000',2)) >> 7)) * 127 + 1
    v = ((bottomHalf + 1) * 16) if bottomHalf != 15 else 255
  else:
    # white, grey, black
    h = 0
    s = 0
    v = bottomHalf * 17

  return (h,s,v)

paletteArray = []
palette565Array = []
  
img = Image.new("RGB",(16,16),"black")
pixels = img.load()

print("palette RGB values:")

for i in range(256):
  hsv = paletteIndexToHSV(i)

  rgb = colorsys.hsv_to_rgb(hsv[0] / 255.0,hsv[1] / 255.0,hsv[2] / 255.0)
  rgb = (clampToByte(rgb[0] * 255),clampToByte(rgb[1] * 255),clampToByte(rgb[2] * 255))

  x = i % 16
  y = int(i / 16)

  print("  " + str(i) + ": " + str(rgb))

  paletteArray.append(rgb[0])
  paletteArray.append(rgb[1])
  paletteArray.append(rgb[2])

  pixels[x,y] = rgb

img.save("palette.png")

print("\npalette RGB565 values:")

colorSet = set()

i = 0

for y in range(16):
  for x in range(16):
    rgb = pixels[x,y]

    rgb565 = rgbTo565(rgb[0],rgb[1],rgb[2])

    if rgb565 in colorSet:
      # If we ge a duplicit color, alter it to get a unique one
      
      found = False

      for r2 in range(-2,5):
        for b2 in range(-2,5):
          for g2 in range(-2,5):
            new565 = rgbTo565(
              clampToByte(rgb[0] + r2),
              clampToByte(rgb[1] + g2),
              clampToByte(rgb[2] + b2))
 
            if not(new565 in colorSet):
              rgb565 = new565
              found = True
              break
          if found:
            break
        if found:
          break
       
      if not(found):
        print("DUPLICITE COLOR!")

    colorSet.add(rgb565)

    palette565Array.append(rgb565)
    print("  " + str(i) + ": " + str(rgb565))

    pixels[x,y] = rgbFrom565(rgb565)

    i += 1

img.save("palette565.png")

print("const uint8_t palette[256 * 3] = {")

text = textwrap.wrap(str(paletteArray)[1:-1],80)

for line in text:
  print(line)

print("}\n\nconst uint16_t palette565[256] = {")

text = textwrap.wrap(str(palette565Array)[1:-1],80)

for line in text:
  print(line)

print("}")
